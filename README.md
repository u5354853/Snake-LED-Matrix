Snake for Arduino
=================

Runs on the Arduino Duemilanove hooked to the 32x16 LED panel
in the CSSA. Uses the serial port at 115200 baud to move
the snake (WASD to change direction of snake)

Open serial port with

picocom /dev/ttyUSB0 --baud 115200

Snake works, keeps track of score and ends game when the snake
collides with itself or it slithers off the end of the world.
Code will display your score at the end of the game, saving high
scores into EEPROM not yet implemented.

--
David Quarel 
25/9/16
