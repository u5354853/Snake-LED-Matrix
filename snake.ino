#include "SPI.h"      
#include "DMD.h" 
#include "TimerOne.h"
#include "point.h"
#include "SystemFont5x7.h"
#include "EEPROM.h"
#include "Arial_Black_16_ISO_8859_1.h"

#define DISPLAYS_ACROSS 1   
#define DISPLAYS_DOWN 1

#define HEIGHT 16
#define WIDTH 32

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
//#define HALT 4

#define UP_JOY 'w'
#define DOWN_JOY 's'
#define LEFT_JOY 'a'
#define RIGHT_JOY 'd'

//#define SPEED_UP 'j'
//#define SPEED_DOWN 'k'

#define MASKTYPE 0x03
#define MASKDICT 0x0C
#define FLASHTICK 50
/* change these values if you have more than one DMD connected */
DMD dmd(DISPLAYS_ACROSS,DISPLAYS_DOWN);

void ScanDMD()
{ 
  dmd.scanDisplayBySPI();
}


void setup()
{
   Timer1.initialize( 5000 );           
/*period in microseconds to call ScanDMD. Anything longer than 5000 (5ms) and you can see flicker.*/

   Timer1.attachInterrupt( ScanDMD );  
/*attach the Timer1 interrupt to ScanDMD which goes to dmd.scanDisplayBySPI()*/

   dmd.clearScreen( true );            
/* true is normal (all pixels off), false is negative (all pixels on) */
}

//####################################################
//####################################################

/*
Snek
David Quarel 25/9/16

World is an array 32x16 = 512 bytes.
Each cell stores the type of cell, and if it is a snek cell,
in what direction the next snek cell lives

B7 B6 B5 B4 B3 B2 B1 B0

B1 B0 - Cell type
0   0 - Empty
0   1 - Snek
1   0 - Fruit
1   1 - Unused

B2 B3 - Direction to next snek cell
0   0 - Up
0   1 - Down
1   0 - Left
1   1 - Right 

Rest of bytes unused
*/

//####################################################
//####################################################

/*
Functions for points, typedef of points
Points represent a pointinate (x,y) in the world
*/


void shift_point(char direct, point *p)
{
    switch (direct){
        case 0: //up
            p -> y --;
            break;
        case 1: //down
            p -> y ++;
            break;
        case 2: //left
            p -> x --;
            break;
        case 3: //right
            p -> x ++;
            break;
    }
}

void mod_point(point *p)
{
    if (p->x < 0) p->x += WIDTH;
    if (p->x >= WIDTH) p->x -= WIDTH;
    if (p->y < 0) p->y += HEIGHT;
    if (p->y >= HEIGHT) p->y -= HEIGHT;

}

//####################################################
//####################################################

bool game_running;

#define GAMEOVERTICK 250


// //read null-terminated string from EEPROM starting
// //at memory location 'addr'
// void EEPROM_read_string(char *ptr, int addr)
// {
//     for(int i = 0; true; i++)
//     {
//         int value = EEPROM.read(addr + i);
//         ptr[i] = value;
//         if(value == '\0')
//             return;
//     }

// }


// //write null-terminated string into EEPROM
// //at memory location 'addr'
// void EEPROM_write_string(char *ptr, int addr)
// {
//     for(int i = 0; true; i++)
//     {
//         EEPROM.write(addr + i,ptr[i]);
//         if(ptr[i] == '\0')
//             return;
//     }
// }

void flushBuffer(){
    while(Serial.available() > 0){
        char temp = Serial.read();
    }
}

void waitForUser(){
    while(Serial.available() == 0){};
}

#define DRAWTEXTTICK 30

void drawText(String dispString) 
{
  dmd.clearScreen( true );
  dmd.selectFont(SystemFont5x7);
  char newString[256];
  int sLength = dispString.length();
  dispString.toCharArray( newString, sLength+1 );
  dmd.drawMarquee(newString,sLength,( 32*DISPLAYS_ACROSS )-1 , 0 );
  long start=millis();
  long timer=start;
  long timer2=start;
  boolean ret=false;
  while(!ret){
    if ( ( timer+ DRAWTEXTTICK ) < millis() ) {
      ret=dmd.stepMarquee( -1 , 0 );
      timer=millis();
    }
  }
}

//#define SCORE_EEPROM_ADDR 100
//#define NAME_EEPROM_ADDR 101


void intro(){
    dmd.clearScreen( true );
    dmd.selectFont(Arial_Black_16_ISO_8859_1);
    // Français, Österreich, Magyarország
    drawText("SNAKE by David Quarel");
   //dmd.drawMarquee(MSG,strlen(MSG),(32*DISPLAYS_ACROSS)-1,0);
}

void game_over(int score)
{
    game_running = false;
    long timer = millis();
    int flash_count = 4;
    bool flash = false;
    //flash the whole screen to indicate game over
    while(flash_count > 0)
    {
        if((timer+GAMEOVERTICK) < millis()) 
        {
           dmd.clearScreen(flash);
           flash = !flash;
           flash_count --;
           timer=millis();
        }
    }
    dmd.clearScreen(true);
    dmd.selectFont(Arial_Black_16_ISO_8859_1);
    drawText("GAME OVER");
    dmd.clearScreen(true);
    dmd.drawString( 0,0, "SCORE", 5, GRAPHICS_NORMAL );
    char buf [4];
    sprintf (buf, "%03i", score);
    dmd.drawString( 0,8, buf, strlen(buf), GRAPHICS_NORMAL );
    flushBuffer();
    waitForUser();   
}


long start=millis();
long gametimer=start;
long flashtimer=start;
unsigned char direct;
unsigned char taildirect;
char temp;
bool fruit_flash;
int GAMETICK = 100;
int score;
unsigned char world[32][16];
bool loop_around;
bool read_serial;
void loop()
{
    Serial.begin(115200);
    game_running = true;
    direct = RIGHT;
    score = 0;
    loop_around = false;

    //empty the world
    for(int i=0; i<32; i++){
        for(int j=0; j<15; j++){
            world[i][j] = 0;
        }
    }

    intro();
    flushBuffer();
    //snek starts at (4,8), three long
    dmd.clearScreen(true);
    point head = {4,8};
    point tail = {2,8};
    //initalise snek in world
    world[2][8] = 0b00001111;
    world[3][8] = 0b00001111;
    world[4][8] = 0b00001110; //unknown direction
    dmd.drawLine(2,8,4,8,GRAPHICS_NORMAL);


    point fruit = {24,8};
    world[fruit.x][fruit.y]=0b00000010; //fruit
    //draw fruit
    dmd.writePixel(fruit.x,fruit.y,GRAPHICS_NORMAL,1);

   
    //wait until player starts game
    waitForUser();
    //seed random number generator on user key press
    randomSeed(millis());
    

    //MAIN GAME LOOP
    while(game_running)
    {

        //read direction to move snek
        if( Serial.available() > 0 && read_serial){
            temp = Serial.read(); //read in joystick control ASCII wasd
            switch(temp){
                case UP_JOY:
                    if(direct != DOWN)
                        direct = UP;
                    break;
                case DOWN_JOY:
                    if(direct != UP)
                        direct = DOWN;
                    break;
                case LEFT_JOY:
                    if(direct != RIGHT)
                        direct = LEFT;
                    break;
                case RIGHT_JOY:
                    if(direct != LEFT)
                        direct = RIGHT;
                    break;
            }
            read_serial = false;
        }


        //Update game every GAMETICK
        if(gametimer + GAMETICK < millis())
        {
            //set direction of old head
            world[head.x][head.y] = (world[head.x][head.y] & ~MASKDICT) | (direct << 2);

            //move snek
            shift_point(direct,&head);
            if(loop_around)
                mod_point(&head);
            //did the snek leave the world?
            else if(head.x < 0 || head.x >= WIDTH || head.y < 0 || head.y >= HEIGHT)
            {
                game_over(score);
                continue; 
                //forgive me padre for I have sinned
            }

            //draw snek head
            dmd.writePixel(head.x, head.y, GRAPHICS_NORMAL, 1);

            //where did the head of the snek land?
            switch(world[head.x][head.y] & MASKTYPE)
            {
                case 0: //empty space,
                    //new snek head
                    world[head.x][head.y] = 1; 
                    //direction to new snake tail?
                    taildirect = (world[tail.x][tail.y] & MASKDICT) >> 2;
                    //erase old snake tail
                    world[tail.x][tail.y] = 0;
                    dmd.writePixel(tail.x,tail.y,GRAPHICS_NORMAL,0);
                    //update snake tail
                    shift_point(taildirect, &tail);
                    
                    if(loop_around) mod_point(&tail);

                    break;

                case 1: //snake ate himself
                    game_over(score);
                    continue;
                    break;

                case 2: //snake ate fruit, don't update tail, generate new fruit
                    //draw new snek head
                    world[head.x][head.y] = 1;
                    //randomly place fruit until it doesn't land on snek
                    do {
                    fruit.x = random(WIDTH);
                    fruit.y = random(HEIGHT);
                    } while(world[fruit.x][fruit.y] != 0);
                    world[fruit.x][fruit.y] = 2;
                    //draw fruit
                    dmd.writePixel(fruit.x,fruit.y,GRAPHICS_NORMAL,1);
                    fruit_flash = true;
                    score += 1;
            }    
            //update interrupt timer for each gametick
            gametimer = millis(); 
            // Serial.print("Head: ");
            // Serial.print(head.x);
            // Serial.print(",");
            // Serial.print(head.y);
            // Serial.print("Tail: ");
            // Serial.print(tail.x);
            // Serial.print(",");
            // Serial.print(tail.y);
            // Serial.print(",");
            // Serial.println(taildirect);
            flushBuffer();
            read_serial = true;
        }

        //make fruit flash
        if(flashtimer + FLASHTICK < millis())
        {
            dmd.writePixel(fruit.x,fruit.y,GRAPHICS_NORMAL,fruit_flash);
            fruit_flash = !fruit_flash;
            flashtimer = millis();
        }
    }
    
}